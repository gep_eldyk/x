<?php


$_1st_part = file_get_contents("./inc/1st_part.json");
$_2nd_part = file_get_contents("./inc/2nd_part.json");
$_3rd_part = file_get_contents("./inc/3rd_part.json"); 

$menu_1st_part = json_decode($_1st_part, true);
$menu_2nd_part = json_decode($_2nd_part, true);
$menu_3rd_part = json_decode($_3rd_part, true);


?>

<div style="background-image:url(./images/blue_back.png); background-position:top;">

    <div class="table-wrapper">
        <h2 style="text-align:center; margin-top: 1.4em; color:#fff !important;">ΜΕΝΟΥ ΤΑΒΕΡΝΑΣ</h2>
    </div>

    <?php
    
// 1st part of menu with first dishes and salads

    foreach ($menu_1st_part as $key_1 => $value_1) {  
        echo '
    <div id="' . $key_1 . '">
        <div class="table-wrapper">
        <br>
        ';

        foreach ($value_1 as $key_2 => $value_2) {
            echo '
        <h4 style="text-align:center; margin-top:3em; margin-bottom:1em; color:#fff !important;">' . $key_2 . '</h4>
            <table class="table_tav">
                
                <thead style="color:#fff !important;">
                    <tr>
                        <th>Είδος</th>
                        <th><div align="center">Τιμή</div></th>
                    </tr>
                </thead>
                <tbody> ';

            foreach ($value_2 as $key_3 => $value_3) {
                echo '
                    <tr>
                        <td>' . $key_3 . '</td>
                        <td><div align="center">' . $value_3 . '</div></td>
                    </tr>';
            }
            echo ' 
                    </tbody>
            </table>';
        }
        echo ' 
        </div>
    </div>';
    } 

// 2nd part of menu with 3 prices per item

    foreach ($menu_2nd_part as $key_1 => $value_1) {

        echo '
    <div id="' . $key_1 . '">
        <div class="table-wrapper">
        <br>
        ';

        foreach ($value_1 as $key_2 => $value_2) {
            echo '
        <h4 style="text-align:center; margin-top:3em; margin-bottom:1em; color:#fff !important;">' . $key_2 . '</h4>
            <table class="table_tav">
                
            <thead>
                <tr>
                    <th>Είδος</th>
                    <th><div align="center">Άνευ</div></th>
                    <th><div align="center">Ρύζι</div></th>
                    <th><div align="center">Πατάτες</div></th>
                </tr>
            </thead>
                <tbody> ';

            foreach ($value_2 as $key_3 => $value_3) {
                echo '
                    <tr>
                        <td>' . $key_3 . '</td>
                        '; 
                        foreach ($value_3 as $key_4 => $value_4) {
                            
                        echo '
                        <td><div align="center">' . $value_4 . '</div></td>';}
                    echo '</tr>';
            }
            echo ' 
                    </tbody>
            </table>';
        }
        echo ' 
        </div>
    </div>';
    }

// 3rd part of menu with sweets / drinks
    
    foreach ($menu_3rd_part as $key_1 => $value_1) { 

        echo '
    <div id="' . $key_1 . '">
        <div class="table-wrapper">
        <br>
        ';

        foreach ($value_1 as $key_2 => $value_2) {
            echo '
        <h4 style="text-align:center; margin-top:3em; margin-bottom:1em; color:#fff !important;">' . $key_2 . '</h4>
            <table class="table_tav">
                
                <thead style="color:#fff !important;">
                    <tr>
                        <th>Είδος</th>
                        <th><div align="center">Τιμή</div></th>
                    </tr>
                </thead>
                <tbody> ';

            foreach ($value_2 as $key_3 => $value_3) {
                echo '
                    <tr>
                        <td>' . $key_3 . '</td>
                        <td><div align="center">' . $value_3 . '</div></td>
                    </tr>';
            }
            echo ' 
                    </tbody>
            </table>';
        }
        echo ' 
        </div>
    </div>';
    }
    ?>

</div>