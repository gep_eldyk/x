<?php


$kafeteria = file_get_contents("./inc/kafeteria.json");

$_menu_kafeterias = json_decode($kafeteria, true);


?>


<div style="background-image:url(./images/blue_back.jpg); background-position:top; ">

    <div class="table-wrapper">
        <h2 style="text-align:center; margin-top: 1.4em; color:#fff !important;">MENOY ΚΑΦΕΤΕΡΙΑΣ</h2>
    </div>

    <?php

    // menu render of kafeteria

    foreach ($_menu_kafeterias as $key_1 => $value_1) { 
        echo '
        <div id="' . $key_1 . '">
            <div class="table-wrapper">
            <br>
            ';

        foreach ($value_1 as $key_2 => $value_2) {
            echo '
            <h4 style="text-align:center; margin-top:3em; margin-bottom:1em; color:#fff !important;">' . $key_2 . '</h4>
                <table class="table_tav">
                    
                    <thead style="color:#fff !important;">
                        <tr>
                            <th>Είδος</th>
                            <th><div align="center">Τιμή</div></th>
                        </tr>
                    </thead>
                    <tbody> ';

            foreach ($value_2 as $key_3 => $value_3) {
                echo '
                        <tr>
                            <td>' . $key_3 . '</td>
                            <td><div align="center">' . $value_3 . '</div></td>
                        </tr>';
            }
            echo ' 
                        </tbody>
                </table>';
        }
        echo ' 
            </div>
        </div>';
    }
    ?>


</div>