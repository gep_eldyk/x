<?php //Header including Nav ?>
					
                    
<header id="header">

	<!-- Logo -->
	<div class="logo">
		<a href="qr.php" class="title"><strong>ΕΛΔΥΚ</strong> 
		<span class="extra">ΛΕΣΧΗ ΑΞΙΩΜΑΤΙΚΩΝ</span></a>
	</div>

	<!-- Nav -->
	<nav id="nav">
		<ul>
			<li><a href="qr.php">Αρχική</a></li>
			<li>
				<a href="bar.php" class="dropdown" >Καφετέρια</a>
				<ul>
					<li><a href="bar.php#rofimata">Ροφήματα</a></li>
					<li><a href="bar.php#liquids">Χυμοί - Αναψυκτικά - Ποτά</a></li>
                    <li><a href="bar.php#foods">Φαγητά</a></li>
					<li><a href="bar.php#ice_creams">Παγωτά</a></li>
				</ul>
			</li>
			<li>
				<a href="restaurant.php" class="dropdown">Εστιατόριο</a>
				<ul>
					<li><a href="restaurant.php#orektika">Ορεκτικά - Σαλάτες</a></li>
					<li><a href="restaurant.php#makaronia">Μακαρονάδες</a></li>
					<li><a href="restaurant.php#psita">Ψητά της ώρας</a></li>
                    <li><a href="restaurant.php#seafood">Ψάρια - Θαλασσινά</a></li>													
					<li><a href="restaurant.php#poikilies">Ποικιλίες</a></li>													
					<li><a href="restaurant.php#glyka">Γλυκά</a></li>													
					<li><a href="restaurant.php#liquids">Αναψυκτικά - Νερά - Ποτά</a></li> 
				</ul>
			</li>
			<!-- <li><a href="uc.php">Ωράριο & Κανόνες Λειτουργίας</a></li> -->
		</ul>
	</nav>

</header>