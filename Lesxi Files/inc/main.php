<?php //Main content ?>

<script>  
	// function myFunction() { 
	// 	document.getElementById("vid").removeAttribute("hidden");
	// } 
</script>

<section id="banner">

	<article class="full" onclick="myFunction()">
		<img src="images/pic01.jpg" alt="" data-position="center" />
		<div class="content" >
			<!-- <video id="vid" hidden  width="300px" controls>   
				<source src="./inc/video.mp4" type="video/mp4" >  
			</video> -->
			<h2><a id="cl" href="#" >ΕΛΔΥΚ</a></h2>
			<p style="font-weight:bold;">Καλώς ήλθατε στην Λέσχη Αξιωματικών της ΕΛΔΥΚ.</p>
			 
		</div>
	</article>
 
	<article class="half">
		<img src="images/pic02.jpg" alt="" data-position="center" />
		<div class="content">
			<h2><a href="bar.php">ΚΑΦΕΤΕΡΙΑ</a></h2>
			<p></p>
			<p style="font-weight:bold;">Μια μεγάλη ποικιλία ροφημάτων, χυμών, αναψυκτικών και ποτών βρίσκεται στην διάθεσή σας για να περάσετε ευχάριστα την ώρα σας.</p>
		</div>
	</article>

	<article class="half">
		<img src="images/pic03.jpg" alt="" data-position="center" />
		<div class="content">
			<h2><a href="restaurant.php">ΕΣΤΙΑΤΟΡΙΟ</a></h2>
			<p></p>
			<p style="font-weight:bold;">Δοκιμάστε τα εδέσματα και τις νοστημιές μας φτιαγμένα με επιλεγμένα υλικά, από τους καλύτερους μάγειρες που διαθέτει η Δύναμη.</p>
		</div>
	</article>

</section>